# __init__.py
# Pour les personnes qui veulent savoir ce que signifie __name__ une démonstration se trouve ici :
# https://www.studytonight.com/python/_name_-as-main-method-in-python
# C'est une chaîne de caractère qui permet de savoir si on exécute le code comme script principal
# appelé directement avec Python et pas importé.
from flask import Flask, render_template, flash, redirect, url_for, session, request, send_from_directory, jsonify
# from flask import Flask, flash, render_template
from Code_Python.APP_COMPUTERS.DATABASE import connect_db_context_manager

# Objet qui fait "exister" notre application
obj_mon_application = Flask(__name__, template_folder="templates")
# Flask va pouvoir crypter les cookies
obj_mon_application.secret_key = '_vogonAmiral_)?^'

# JC 2020.04.21 Tout commence ici par "indiquer" les routes de l'application
from Code_Python.APP_COMPUTERS import routes
from Code_Python.APP_COMPUTERS.ADMIN import routes_gestion_admin
from Code_Python.APP_COMPUTERS.COMPUTERS import routes_gestion_computers
from Code_Python.APP_COMPUTERS.PEOPLE import routes_gestion_people