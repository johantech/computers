# routes_gestion_people.py
# JC 2020.04.22 Gestion des routes flask pour la table intermédiaire qui associe les administrateurs et les types
from flask import render_template, request, flash, session
from Code_Python.APP_COMPUTERS import obj_mon_application
from Code_Python.APP_COMPUTERS.PEOPLE.data_gestion_people import GestionPeople
from Code_Python.APP_COMPUTERS.COMPUTERS.data_gestion_computers import GestionComputers


# -------------------------------------------------------------------------------------------------------
# Définition d'une route pour /computers_afficher.html
# cela va permettre de programmer els actions avant d'intéragir
# avec le navigateur par la méthode render_template
# Pour tester http://127.0.0.1/type_exercice_afficher
# -------------------------------------------------------------------------------------------------------
@obj_mon_application.route("/type_exercice_afficher_concat/<int:id_exercice_sel>", methods=['GET', 'POST'])
def type_exercice_afficher_concat(id_exercice_sel):
    print("id_exercice_sel : ", id_exercice_sel)
    if request.method == "GET":
        try:
            obj_actions_type = GestionPeople()
            # récupère les données grâce à la request SQL générée dans la classe ci-dessus
            # fichier data_gestion_people.py
            data_type_exercice_afficher_concat = obj_actions_type.type_exercice_afficher_data_concat(id_exercice_sel)
            print(" data people ", data_type_exercice_afficher_concat, " people ", type(data_type_exercice_afficher_concat))

            # différencier les messages si la table est vide
            if data_type_exercice_afficher_concat:
                # rassurer l'utilisateur
                flash(f'Données types affichées dans TypeExercice', 'success')
                print("if  people-ex")
            else:
                flash(f"""L'exercice demandé n'existe pas, ou la table "t_avoirexercice" est vide !!! """, "warning")
                print("else  people-ex")
        except Exception as erreur:
            print(f"RGTE Erreur générale")
            # message personnalisé
            raise Exception(f"RGTE Erreur générale. {erreur}")
    # Envoie la page html au serveur
    print("before render-template people-ex")
    return render_template("computers/computers_afficher.html", data=data_type_exercice_afficher_concat)
    print("after render")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_genre_film_selected
# Récupère la liste de tous les genres du film sélectionné.
# Nécessaire pour afficher tous les "TAGS" des genres, ainsi l'utilisateur voit les genres à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_edit_type_exercice_selected", methods=['GET', 'POST'])
def gf_edit_type_exercice_selected():
    if request.method == "GET":
        try:
            print("try gf_edit_type_exercice_selected")
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_type = GestionTypePeople()
            print("GestionTypePeople()")
            # Récupère les données grâce à une requête MySql définie dans la classe GestionGenres()
            # Fichier data_gestion_genres.py
            # Pour savoir si la table "t_genres" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(genres_films_modifier_tags_dropbox.html)
            print("before data people all")
            data_types_all = obj_actions_type.type_afficher_data()
            print("data people all : ", data_types_all)
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_types = GestionTypeExercice()
            print("GestionTypeExercice()")
            # OM 2020.04.21 Récupère la valeur de "id_film" du formulaire html "genres_films_afficher.html"
            # l'utilisateur clique sur le lien "Modifier genres de ce film" et on récupère la valeur de "id_film" grâce à la variable "id_film_genres_edit_html"
            # <a href="{{ url_for('gf_edit_genre_film_selected', id_film_genres_edit_html=row.id_film) }}">Modifier les genres de ce film</a>
            id_exercice_type_edit = request.values['id_exercice_type_edit_html']
            print("récupération de l'ex pour modif people")
            # OM 2020.04.21 Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_exercice_type_edit'] = id_exercice_type_edit
            print("création de la variable de session")
            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_exercice_selected_dictionnaire = {"value_id_exercice_selected": id_exercice_type_edit}
            print("création du dictionnaire")
            print(valeur_id_exercice_selected_dictionnaire)
            # Récupère les données grâce à 3 requêtes MySql définie dans la classe GestionGenresFilms()
            # 1) Sélection du film choisi
            # 2) Sélection des genres "déjà" attribués pour le film.
            # 3) Sélection des genres "pas encore" attribués pour le film choisi.
            # Fichier data_gestion_genres_films.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "genres_films_afficher_data"
            data_type_exercice_selected, data_type_exercice_non_attribues, data_type_exercice_attribues = \
                obj_actions_types.type_exercice_afficher_data(valeur_id_exercice_selected_dictionnaire)
            print("récupération des valeurs grâce au CRUD de GestionTypeExercice")
            lst_data_exercice_selected = [item['id_Exercice'] for item in data_type_exercice_selected]
            # DEBUG bon marché : Pour afficher le résultat et son people.
            print("lst_data_exercice_selected  ", lst_data_exercice_selected,
                  type(lst_data_exercice_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui ne sont pas encore sélectionnés.
            lst_data_type_exercice_non_attribues = [item['id_TypeExercice'] for item in data_type_exercice_non_attribues]
            session['session_lst_data_type_exercice_non_attribues'] = lst_data_type_exercice_non_attribues
            # DEBUG bon marché : Pour afficher le résultat et son people.
            print("lst_data_type_exercice_non_attribues  ", lst_data_type_exercice_non_attribues,type(lst_data_type_exercice_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui sont déjà sélectionnés.
            lst_data_type_exercice_old_attribues = [item['id_TypeExercice'] for item in data_type_exercice_attribues]
            session['session_lst_data_type_exercice_old_attribues'] = lst_data_type_exercice_old_attribues
            # DEBUG bon marché : Pour afficher le résultat et son people.
            print("lst_data_type_exercice_old_attribues  ", lst_data_type_exercice_old_attribues, type(lst_data_type_exercice_old_attribues))

            # DEBUG bon marché : Pour afficher le résultat et son people.
            print(" data data_type_exercice_selected", data_type_exercice_selected, "people ",type(data_type_exercice_selected))
            print(" data data_type_exercice_non_attribues ", data_type_exercice_non_attribues, "people ",type(data_type_exercice_non_attribues))
            print(" data_type_exercice_attribues ", data_type_exercice_attribues, "people ",type(data_type_exercice_attribues))

            # Extrait les valeurs contenues dans la table "t_genres", colonne "intitule_genre"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_genre
            lst_data_type_exercice_non_attribues = [item['Type_TypeExercice'] for item in data_type_exercice_non_attribues]
            # DEBUG bon marché : Pour afficher le résultat et son people.
            print("lst_all_genres gf_edit_type_exercice_selected ", lst_data_type_exercice_non_attribues,type(lst_data_type_exercice_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_exercice_selected == [None]:
                flash(f"""L exercice demandé n existe pas. Ou la table "t_avoirtype" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données types affichées dans TypeExercice!!", "success")

        except Exception as erreur:
            print(f"A RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"A RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("computers/computers_modifier_tags_dropbox.html",
                           data_type=data_types_all,
                           data_exercice_selected=data_type_exercice_selected,
                           data_type_attribues=data_type_exercice_attribues,
                           data_type_non_attribues=data_type_exercice_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_genre_film_selected
# Récupère la liste de tous les genres du film sélectionné.
# Nécessaire pour afficher tous les "TAGS" des genres, ainsi l'utilisateur voit les genres à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_update_type_exercice_selected", methods=['GET', 'POST'])
def gf_update_type_exercice_selected():
    print("GF UPDATE COMPUTERS ADMIN SELECTED")
    if request.method == "POST":
        try:
            # print("Récupère l'id du film sélectionné")
            id_exercice_selected = session['session_id_exercice_type_edit']
            print("session['session_id_exercice_type_edit'] ", session['session_id_exercice_type_edit'])

            # print("Récupère la liste des genres qui ne sont pas associés au film sélectionné.")
            old_lst_data_type_exercice_non_attribues = session['session_lst_data_type_exercice_non_attribues']
            print("old_lst_data_type_exercice_non_attribues ", old_lst_data_type_exercice_non_attribues)

            # print("Récupère la liste des genres qui sont associés au film sélectionné.")
            old_lst_data_type_exercice_attribues = session['session_lst_data_type_exercice_old_attribues']
            print("old_lst_data_type_exercice_old_attribues ", old_lst_data_type_exercice_attribues)

            # print("Effacer toutes les variables de session.")
            session.clear()

            # print("Récupère ce que l'utilisateur veut modifier comme genres dans le composant
            # 'tags-selector-tagselect' dans le fichier 'genres_films_modifier_tags_dropbox.html'")
            new_lst_str_type_exercice = request.form.getlist('name_select_tags')
            print("new_lst_str_type_exercice ", new_lst_str_type_exercice)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_type_exercice_old = list(map(int, new_lst_str_type_exercice))
            print("new_lst_str_type_exercice ", new_lst_int_type_exercice_old, "people new_lst_str_type_exercice ",
                  type(new_lst_int_type_exercice_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_genre" qui doivent être effacés de la table intermédiaire "t_genres_films".
            lst_diff_type_delete_b = list(
                set(old_lst_data_type_exercice_attribues) - set(new_lst_int_type_exercice_old))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_type_delete_b ", lst_diff_type_delete_b)

            # OM 2020.04.29 Une liste de "id_genre" qui doivent être ajoutés à la BD
            lst_diff_type_insert_a = list(
                set(new_lst_int_type_exercice_old) - set(old_lst_data_type_exercice_attribues))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_type_insert_a ", lst_diff_type_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_type = GestionTypeExercice()

            # Pour le film sélectionné, parcourir la liste des genres à INSÉRER dans la "t_genres_films".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_type_ins in lst_diff_type_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                # et "id_genre_ins" (l'id du genre dans la liste) associé à une variable.
                valeurs_exercice_sel_type_sel_dictionnaire = {"value_fk_exercice": id_exercice_selected,
                                                              "value_fk_type": id_type_ins}
                # Insérer une association entre un(des) genre(s) et le film sélectionner.
                obj_actions_type.type_exercice_add(valeurs_exercice_sel_type_sel_dictionnaire)

            # Pour le film sélectionné, parcourir la liste des genres à EFFACER dans la "t_genres_films".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_type_del in lst_diff_type_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                # et "id_genre_del" (l'id du genre dans la liste) associé à une variable.
                valeurs_exercice_sel_type_sel_dictionnaire = {"value_fk_exercice": id_exercice_selected,
                                                              "value_fk_type": id_type_del}
                # Effacer une association entre un(des) genre(s) et le film sélectionner.
                obj_actions_type.type_exercice_delete(valeurs_exercice_sel_type_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe GestionGenres()
            # Fichier data_gestion_genres.py
            # Afficher seulement le film dont les genres sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_type_exercice_afficher_concat = obj_actions_type.type_exercice_afficher_data_concat(
                id_exercice_selected)
            # DEBUG bon marché : Pour afficher le résultat et son people.
            print(" data genres", data_type_exercice_afficher_concat, "people ", type(data_type_exercice_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_type_exercice_afficher_concat == None:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_genres_films" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données genres affichées dans GenresFilms!!", "success")

        except Exception as erreur:
            print(f"B RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_genres_films",
    # on affiche les films et le(urs) genre(s) associé(s).
    return render_template("computers/computers_afficher.html",
                           data=data_type_exercice_afficher_concat)
