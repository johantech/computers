#routes_gestion_people.py
#JC 2020.04.22 Gestions des routes pour les types

from flask import render_template, flash, redirect, url_for, request
from Code_Python.APP_COMPUTERS import obj_mon_application
from Code_Python.APP_COMPUTERS.COMPUTERS.data_gestion_computers import GestionComputers
from Code_Python.APP_COMPUTERS.DATABASE.erreurs import *

# import pour utiliser les expressions régulières REGEX
import re

# ------------------------------------------------------------------------------
# Définition d'une route type_afficher
# cela va nous permettre de programmer les actions avant d'intéragir
# avec le navigateur par la méthode "render_template"
# pour tester http://127.0.0.1:1260/computer_afficher
# ------------------------------------------------------------------------------
#définit les routes qui pourront récupérer les infos des forms en GET et POST
@obj_mon_application.route("/computers_afficher", methods=['GET','POST'])
def computers_afficher():
    # on test si les données d'un form sont un envoi de données par des champs de forms ou juste un affichage
    if request.method == "GET":
        try:
            # objet contenant les méthodes CRUD pour gérer les données
            obj_actions_computers = GestionComputers()
            # récupère les données par requête SQL définie dans la classe GestionType()
            # fichier data_gestion_type_admin.py
            data_computers = obj_actions_computers.computers_afficher_data()
            # DEBUG bon marché : pour afficher un message dans la console
            print(f"data_computers : ",data_computers," computers ",type(data_computers))

            # affiche un message pour rassurer les utilisateurs
            flash("Données people affichées !!!", "Success")
        except Exception as erreur:
            print(f"RGT Erreur générale")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGT Exception {erreur}")
            raise Exception(f"RGT Erreur générale {erreur}")
            # raise MaBdErreurOperation(f"RGT Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page au serveur
    return render_template("computers/computers_afficher.html", data=data_computers)
# ------------------------------------------------------------------------------
# Définition d'une route type_add
# cela va nous permettre de programmer les actions avant d'intéragir
# avec le navigateur par la méthode "render_template"
# pour tester http://127.0.0.1:1260/computers_add
# ------------------------------------------------------------------------------
#définit les routes qui pourront récupérer les infos des forms en GET et POST
@obj_mon_application.route("/computers_add", methods=['GET','POST'])
def computers_add():
    # on vérifie si les données sont un affichage ou un envoi par un formulaire HTML
    if request.method=="POST":
        try:
            obj_actions_computers = GestionComputers()

            # Récupération des champs
            serialnumber = request.form['serialnumber_html']
            make = request.form['make_html']

            # pas de nécessité d'insérer une regex pour les administrateurs
            valeurs_insertion_dictionnaire = {"value_serialnumber": serialnumber,
                                              "value_make": make}

            obj_actions_computers.add_computers_data(valeurs_insertion_dictionnaire)

            # rassurer l'utilisateur
            flash(f'Données insérées !!!', 'Success')
            print(f"Données insérées !!!")
            # on va interpréter la route "exercice_afficher" car l'utilisateur doit voir
            # le nouvel admin qu'il vient d'insérer
            return redirect(url_for('computers_afficher'))

        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGT pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur: {erreur}")
            raise MonErreur(
                f"Autre erreur, veuillez arrêter d'utiliser un ordinateur et passer à une machine à écrire + FAX !!!")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGT Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    return render_template("computers/computers_add.html")

# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /type_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un people d'admin par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/computers_edit', methods=['POST', 'GET'])
def computers_edit():
    # les données sont affichiées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "people_afficher.html"
    if request.method=='GET':
        try:
            # Récupérer la valeur de "id_TypeExercice" du formulaire html "people_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_TypeExercice"
            # grâce à la variable "id_type_edit.thml"
            # <a href="{{ url_for('type_edit', id_type_edit_html=row.id_TypeExercice)>Edit</a>
            id_computers_edit = request.values['id_computers_edit_html']

            # Pour afficher dans la console la valeur de "id_type_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print("id_computers_edit BUG : ", id_computers_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_computers": id_computers_edit}

            # Objet contenant toutes les méthodes CRUD pour gérer les données
            obj_actions_computers = GestionComputers()

            # la commande MySql est envoyée à la BD
            data_id_computers = obj_actions_computers.edit_computers_data(valeur_select_dictionnaire)
            print("dataIdComputers ", data_id_computers, " people ", type(data_id_computers))
            # Message permettant de donner un sentiment rassurant aux utilisateurs
            flash(f"Editer le people d'un exercice !!!")
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    return render_template("computers/computers_edit.html", data=data_id_computers)
#-------------------------------------------------------------------------------------------------------------
# Définition d'une route pour type_update, cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode render_template
# On change la valeur d'un people d'admin par la commande MySql "UPDATE"
#-------------------------------------------------------------------------------------------------------------
@obj_mon_application.route("/computers_update", methods=['POST','GET'])
def computers_update():
    # debug bon marché pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "people_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du people alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # debug bon marché pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            # Récupérer la valeur de "id_computers" du formulaire html "computers_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_computers"
            # grâce à la variable "id_computers_edit_html"
            # <a href="{{ url_for('computers_edit', id_computers_edit_html=row.id_computers)>Edit</a>
            id_computers_edit = request.values['name_edit_id_computers_html']

            # Récupère le contenu du champ "intitule_type" dans le formulaire html "people_edit.html"
            name_computers = request.values['name_edit_serialnumber_html']
            dateBuy = request.values['name_edit_datebuy_html']
            name_make = request.values['name_edit_make_html']
            valeur_edit_list = [{'value_id_computers': id_computers_edit,
                                 'value_serialnumber': name_computers,
                                 'value_datebuy': dateBuy,
                                 'value_make': name_make}]
            print("valeur edit list : ",valeur_edit_list)
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence
            if not re.match("(^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$)",name_computers):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "intitule_type" dans le formulaire HTML "TypeEdit.html"
                # name_genre = request.values['name_edit_intitule_type_html']
                # Message (très) humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "Danger")

                # On doit afficher à nouveau le formulaire "genres_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "genres_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_type': 13, 'intitule_type': 'philosophique'}]
                valeur_edit_list = [{'value_id_computers': id_computers_edit,
                                     'value_serialnumber': name_computers,
                                     'value_datebuy': dateBuy,
                                     'value_make': name_make}]

                # Debug bon marché
                # Pour afficher le contenu et le people de valeurs passées au formulaire "computers_edit.html"
                print(valeur_edit_list, "computers ..", type(valeur_edit_list))
                return render_template("computers/computers_edit.html", data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {'value_id_computers': id_computers_edit,
                                              'value_serialnumber': name_computers,
                                              'value_datebuy': dateBuy,
                                              'value_make': name_make}
                # Objet contenant toutes les méthodes CRUD pour gérer les données
                obj_actions_computers = GestionComputers()

                # la commmande MySql est envoyée à la BD
                data_id_computers = obj_actions_computers.update_computers_data(valeur_update_dictionnaire)
                # debug bon marché
                print("dataIdComputers : ", data_id_computers, " computers : ", type(data_id_computers))
                # message rassurant à l'utilisateur
                flash(f"Editer l'ordinateur d'un client !!!")
                # On affiche les types
                return redirect(url_for('computers_afficher'))
        except (Exception,
            pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.IntegrityError,
            TypeError) as erreur:

            print(erreur.args)
            flash(f"problème computers update {erreur.args[0]}")
            # en cas de problème, mais surtout en cas de non respect
            # des règles REGEX dans le champ "name_edit_intitule_type_html"
    return render_template('computers/computers_edit.html', data=valeur_edit_list)

    # return render_template("computers/computers_update.html")
#-------------------------------------------------------------------------------------------------------------
# Définition d'une route pour type_select_delete, cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode render_template
# On change la valeur d'un people d'admin par la commande MySql "UPDATE"
#-------------------------------------------------------------------------------------------------------------
@obj_mon_application.route('/type_select_delete', methods=['POST','GET'])
def computers_select_delete():

    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_computers = GestionComputers()
            # OM 2019.04.04 Récupérer la valeur de "idGenreDeleteHTML" du formulaire html "GenresDelete.html"
            id_computers_delete = request.args.get('id_computers_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_computers": id_computers_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_computers = obj_actions_computers.delete_select_computers_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('computers/computers_delete.html', data =data_id_computers)

# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /typeUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un people, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/computer_delete', methods=['POST', 'GET'])
def computers_delete():

    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_computers = GestionComputers()
            # OM 2019.04.02 Récupérer la valeur de "id_TypeExercice" du formulaire html "TypeAfficher.html"
            id_computers_delete = request.form['id_computers_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_computers": id_computers_delete}

            data_computers = obj_actions_computers.delete_computers_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des people d'admin
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les people
            return redirect(url_for('computers_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "people" d'admin' qui est associé dans "t_avoirType".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE de effacer !!! Cette valeur est associée à des utilisateurs !')
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce people est associé à des utilisateurs dans la t_avoirType !!! : {erreur}")
                # Afficher la liste des genres des films
                return redirect(url_for('computers_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur type_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur type_delete {erreur.args[0], erreur.args[1]}")


            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('computers/computers_afficher.html', data=data_computers)

