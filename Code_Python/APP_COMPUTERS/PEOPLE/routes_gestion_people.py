#routes_gestion_people.py
#JC 2020.04.22 Gestions des routes pour les types

from flask import render_template, flash, redirect, url_for, request
from Code_Python.APP_COMPUTERS import obj_mon_application
from Code_Python.APP_COMPUTERS.PEOPLE.data_gestion_people import GestionPeople
from Code_Python.APP_COMPUTERS.DATABASE.erreurs import *

# import pour utiliser les expressions régulières REGEX
import re

# ------------------------------------------------------------------------------
# Définition d'une route type_afficher
# cela va nous permettre de programmer les actions avant d'intéragir
# avec le navigateur par la méthode "render_template"
# pour tester http://127.0.0.1:1260/people_afficher
# ------------------------------------------------------------------------------
#définit les routes qui pourront récupérer les infos des forms en GET et POST
@obj_mon_application.route("/people_afficher", methods=['GET','POST'])
def people_afficher():
    # on test si les données d'un form sont un envoi de données par des champs de forms ou juste un affichage
    if request.method == "GET":
        try:
            # objet contenant les méthodes CRUD pour gérer les données
            obj_actions_people = GestionPeople()
            # récupère les données par requête SQL définie dans la classe GestionType()
            # fichier data_gestion_type_admin.py
            data_people = obj_actions_people.people_afficher_data()
            # DEBUG bon marché : pour afficher un message dans la console
            print(f"data_people : ",data_people," people ",type(data_people))

            # affiche un message pour rassurer les utilisateurs
            flash("Données people affichées !!!", "Success")
        except Exception as erreur:
            print(f"RGT Erreur générale")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGT Exception {erreur}")
            raise Exception(f"RGT Erreur générale {erreur}")
            # raise MaBdErreurOperation(f"RGT Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page au serveur
    return render_template("people/people_afficher.html", data=data_people)
# ------------------------------------------------------------------------------
# Définition d'une route people_add
# cela va nous permettre de programmer les actions avant d'intéragir
# avec le navigateur par la méthode "render_template"
# pour tester http://127.0.0.1:1260/people_add
# ------------------------------------------------------------------------------
# définit les routes qui pourront récupérer les infos des forms en GET et POST
@obj_mon_application.route("/people_add", methods=['GET','POST'])
def people_add():
    # on vérifie si les données sont un affichage ou un envoi par un formulaire HTML
    if request.method =="POST":
        try:
            # objet qui contient les méthodes CRUD pour la gestion des données
            obj_actions_people = GestionPeople()
            # récupère le contenu du champ dans le formulaires HTML "people_add.html"
            name_people = request.form['people_afficher_html']

            # on ne doit pas accepter de valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres
            # Accepte le trait d'union ou l'apostrophe et l'espace entre deux mots mais pas plus d'une occurence
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF]*['\\- ]?[A-Za-z\u00C0-\u00FF]+$",
                            name_people):
                # Message humiliant à l'utilisateur
                flash(f"Une entréee...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double,"
                      f"de double apostrophe, de double trait union et ne doit pas être vide", "Danger")
                # On doit afficher à nouveau le formulaire "people_add.html" à cause des erreurs de "claviotage"
                return render_template("people/people_add.html")
            else:
                valeurs_insertion_dictionnaire = {"value_intitule_people": name_people}
                obj_actions_people.add_people_data(valeurs_insertion_dictionnaire)

                # les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées !!", "Success")
                print(f"Données insérées !!")
                # on va interpréter éa route "type_afficher" car l'utilisateur doit voir le nouveau people qu'il
                # vient d'insérer
                return redirect(url_for('people_afficher'))
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGT pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGT Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("people/people_add.html")#, data=data_one)

# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /type_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un people d'admin par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/people_edit', methods=['POST', 'GET'])
def people_edit():
    # les données sont affichiées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "people_afficher.html"
    if request.method=='GET':
        try:
            # Récupérer la valeur de "id_TypeExercice" du formulaire html "people_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_TypeExercice"
            # grâce à la variable "id_type_edit.thml"
            # <a href="{{ url_for('type_edit', id_type_edit_html=row.id_TypeExercice)>Edit</a>
            id_people_edit = request.values['id_people_edit_html']

            # Pour afficher dans la console la valeur de "id_type_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print("id_people_edit BUG : ", id_people_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_people": id_people_edit}

            # Objet contenant toutes les méthodes CRUD pour gérer les données
            obj_actions_people = GestionPeople()

            # la commande MySql est envoyée à la BD
            data_id_people = obj_actions_people.edit_people_data(valeur_select_dictionnaire)
            print("dataIdPeople ", data_id_people, " people ", type(data_id_people))
            # Message permettant de donner un sentiment rassurant aux utilisateurs
            flash(f"Editer le people !!!")
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    return render_template("people/people_edit.html", data=data_id_people)
#-------------------------------------------------------------------------------------------------------------
# Définition d'une route pour type_update, cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode render_template
# On change la valeur d'un people d'admin par la commande MySql "UPDATE"
#-------------------------------------------------------------------------------------------------------------
@obj_mon_application.route("/people_update", methods=['POST','GET'])
def people_update():
    # debug bon marché pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "people_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du people alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # debug bon marché pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            # Récupérer la valeur de "id_computers" du formulaire html "computers_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_computers"
            # grâce à la variable "id_computers_edit_html"
            # <a href="{{ url_for('computers_edit', id_computers_edit_html=row.id_computers)>Edit</a>
            id_people_edit = request.values['id_people_edit_html']

            # Récupère le contenu du champ "intitule_type" dans le formulaire html "people_edit.html"
            name_people = request.values['name_edit_intitule_people_html']
            valeur_edit_list = [{'id_people': id_people_edit, 'intitule_people': name_people}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence --> le REGEX!!!!!
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF]*['\\- ]?[A-Za-z\u00C0-\u00FF]+$", name_people):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "intitule_type" dans le formulaire HTML "TypeEdit.html"
                # name_genre = request.values['name_edit_intitule_type_html']
                # Message (très) humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "Danger")

                # On doit afficher à nouveau le formulaire "genres_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "genres_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_type': 13, 'intitule_type': 'philosophique'}]
                valeur_edit_list = [{'id_people': id_people_edit, 'intitule_people': name_people}]

                # Debug bon marché
                # Pour afficher le contenu et le people de valeurs passées au formulaire "computers_edit.html"
                print(valeur_edit_list, "people ..", type(valeur_edit_list))
                return render_template("people/people_edit.html", data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_people": id_people_edit, "value_name_people": name_people}
                # Objet contenant toutes les méthodes CRUD pour gérer les données
                obj_actions_people = GestionPeople()

                # la commmande MySql est envoyée à la BD
                data_id_people = obj_actions_people.update_people_data(valeur_update_dictionnaire)
                # debug bon marché
                print("dataIdPeople : ", data_id_people, " people : ", type(data_id_people))
                # message rassurant à l'utilisateur
                flash(f"Editer un client !!!")
                # On affiche les types
                return redirect(url_for('people_afficher'))
        except (Exception,
            pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.IntegrityError,
            TypeError) as erreur:

            print(erreur.args)
            flash(f"problème people update {erreur.args[0]}")
            # en cas de problème, mais surtout en cas de non respect
            # des règles REGEX dans le champ "name_edit_intitule_people_html"
    return render_template('people/people_edit.html', data=valeur_edit_list)

    # return render_template("computers/computers_update.html")
#-------------------------------------------------------------------------------------------------------------
# Définition d'une route pour type_select_delete, cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode render_template
# On change la valeur d'un people d'admin par la commande MySql "UPDATE"
#-------------------------------------------------------------------------------------------------------------
@obj_mon_application.route('/people_select_delete', methods=['POST','GET'])
def people_select_delete():

    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_people = GestionPeople()
            # OM 2019.04.04 Récupérer la valeur de "idGenreDeleteHTML" du formulaire html "GenresDelete.html"
            id_people_delete = request.args.get('id_people_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_people": id_people_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_people = obj_actions_people.delete_select_people_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('people/people_delete.html', data =data_id_people)

# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /typeUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un people, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/computer_delete', methods=['POST', 'GET'])
def people_delete():

    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_people = GestionPeople()
            # OM 2019.04.02 Récupérer la valeur de "id_TypeExercice" du formulaire html "TypeAfficher.html"
            id_people_delete = request.form['id_people_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_people": id_people_delete}

            data_people = obj_actions_people.delete_people_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des people d'admin
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les autres gugus
            return redirect(url_for('people_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "people" d'admin' qui est associé dans "t_avoirType".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE de effacer !!! Cette valeur est associée à des utilisateurs !')
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce people est associé à des utilisateurs dans la table people !!! : {erreur}")
                # Afficher la liste des genres des films
                return redirect(url_for('people_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur people_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur people_delete {erreur.args[0], erreur.args[1]}")


            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('people/people_afficher.html', data=data_people)

