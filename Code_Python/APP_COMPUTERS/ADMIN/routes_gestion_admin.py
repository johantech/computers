#routes_gestion_admin.py
# JC 2020.04.21 Gestion des "routes" FLASK pour les administrateurs

import pymysql
from flask import render_template, flash, redirect, url_for, request
from Code_Python.APP_COMPUTERS import obj_mon_application
from Code_Python.APP_COMPUTERS.ADMIN.data_gestion_admin import GestionAdmin
from Code_Python.APP_COMPUTERS.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from Code_Python.APP_COMPUTERS.DATABASE.erreurs import *
import re
# JC 2020.04.21 Afficher un avertissement sympa...mais contraignant
# Pour le tester http://127.0.0.1:1260/avertissement_sympa_pour_geeks
@obj_mon_application.route("/avertissement_sympa_pour_geeks")
def avertissement_sympa_pour_geeks():
    # envoie la page HTML au serveur
    return render_template("admin/avertissement_sympa_pour_geeks.html")




# Afficher les administrateurs
# Pour tester http://127.0.0.1:1260/admin_afficher
@obj_mon_application.route("/admin_afficher")
def admin_afficher():
    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de données par des champs
    # du formulaire HTML
    if request.method == "GET":
        try:
            # objet contenant toutes les méthodes CRUD des données
            obj_actions_admin = GestionAdmin()
            # récupère les données grâce à une requête MySql définie dans GestionAdmin()
            # Fichier data_gestion_admin.py
            data_admin = obj_actions_admin.admin_afficher_data()
            # DEBUG bon marché : pour afficher un message dans la console
            print(" data admin", data_admin," people ",type(data_admin))

            # la ligne suivante permet de donner un sentiment rassurant à l'utilisateur
            flash("Données admin affichées !!", "Success")
        except Exception as erreur:
            print(f"RGF Erreur générale")
            # On dérive "Exception" par le "@obj_mon_application_errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}")
    # Envoie la page au serveur
    return render_template("admin/admin_afficher.html", data=data_admin)

# Ajouter les administrateurs
# Pour tester http://127.0.0.1:1260/admin_add
@obj_mon_application.route("/admin_add", methods=['GET','POST'])
def admin_add():
    # on vérifie si les données sont un affichage ou un envoi par un formulaire HTML
    if request.method=="POST":
        try:
            obj_actions_admin = GestionAdmin()

            # Récupération des champs
            user = request.form['user_html']
            email = request.form['email_html']
            password = request.form['password_html']
            firstName = request.form['firstName_html']
            lastName = request.form['lastName_html']

            # pas de nécessité d'insérer une regex pour les administrateurs
            valeurs_insertion_dictionnaire = {"value_user": user,
                                              "value_email": email,
                                              "value_password": password,
                                              "value_firstName": firstName,
                                              "value_lastName": lastName}

            obj_actions_admin.add_admin_data(valeurs_insertion_dictionnaire)

            # rassurer l'utilisateur
            flash(f'Données insérées !!!', 'Success')
            print(f"Données insérées !!!")
            # on va interpréter la route "exercice_afficher" car l'utilisateur doit voir
            # le nouvel admin qu'il vient d'insérer
            return redirect(url_for('admin_afficher'))

        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGT pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur: {erreur}")
            raise MonErreur(f"Autre erreur, veuillez arrêter d'utiliser un ordinateur et passer à une machine à écrire + FAX !!!")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGT Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    return render_template("admin/admin_add.html")

# Editer un admin
@obj_mon_application.route("/admin_edit", methods=['GET','POST'])
def admin_edit():
    print("admin_edit")
    # les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton edit dans e formulaire "admin_afficher.html"
    if request.method=='GET':
        print("if")
        try:
            print("try")
            # Récupérer la valeur de l'id_admin
            print("prép récup id")
            id_admin_edit = request.values['id_admin_edit_html']
            print("id_admin_edit", id_admin_edit)

            # Dictionnaire et insertion dans la BD
            print("")
            valeur_select_dictionnaire = {"value_id_admin": id_admin_edit}
            print("Dico créé, félicitations vous allez battre Monsieur Maccaud dans les BDD !!")
            obj_actions_admin = GestionAdmin()
            data_id_admin = obj_actions_admin.edit_admin_data(valeur_select_dictionnaire)
            print("dataIDAdmin : ", data_id_admin, " admin : ", type(data_id_admin))

            # rassurer l'utilisateur
            flash(f'Editer un admin')
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    print("before render template")
    return render_template('admin/admin_edit.html', data=data_id_admin)
    print("after render")
# UPDATE un admin (après edit)
@obj_mon_application.route("/admin_update", methods=['GET','POST'])
def admin_update():
    # debug bon marché pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method =='POST':
        try:
            print("avant request values : ",request.values)
             # Récupère la valeur de l'id_admin à updater
            id_admin_edit = request.values['id_admin_edit_html']
            print("apres request values : ", request.values)

            # récupère le contenu des champs
            user = request.values['name_edit_user_html']
            print("apres name edit : ")
            email = request.values['email_edit_email_html']
            print("apres email edit : ")
            password = request.values['password_edit_password_html']
            print("apres password edit : ")
            firstName = request.values['firstName_edit_firstName_html']
            print("apres firstName edit : ")
            lastName = request.values['lastName_edit_lastName_html']
            print("apres lastName edit : ")
            print("apres request values : ")

            # création d'une liste avec le contenu des champs du form "edit_admin.html
            valeur_edit_list = [{'id_admin': id_admin_edit,
                                 'user': user,
                                 'email': email,
                                 'password': password,
                                 'firstName': firstName,
                                 'lastName': lastName}]
            print("admin id :", type(id_admin_edit))
            print("user :", type(user))
            print("password :", type(password))
            print("firstName :", type(firstName))
            print("lastName :", type(lastName))
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence
            if not re.match("(^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$)",user):
                flash('Une entrée incorrecte...!!! Pas de chiffres, pas de caractères spéciaux, d\'espace à double, de double apostrophe'
                      'de double trait d\'union et ne doit pas être vide !!!', 'Danger')
                # On doit afficher à nouveau le formulaire "genres_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "genres_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_type': 13, 'intitule_type': 'philosophique'}]
                valeur_edit_list = [{'id_admin': id_admin_edit,
                                     'user': user,
                                     'email': email,
                                     'password': password,
                                     'firstName': firstName,
                                     'lastName': lastName}]

                # Debug (très) bon marché
                # Pour afficher le contenu et le people de valeurs passées au formulaire "admin_edit.html"
                print(valeur_edit_list, "admin ..", type(valeur_edit_list))
                return render_template("admin/admin_update.html", data=valeur_edit_list)
            else:
                valeur_update_dictionnaire ={'id_admin': id_admin_edit,
                                     'user': user,
                                     'email': email,
                                     'password': password,
                                     'firstName': firstName,
                                     'lastName': lastName}
                obj_actions_admin = GestionAdmin()
                data_id_admin = obj_actions_admin.update_admin_data(valeur_update_dictionnaire)
                print("DataIDadmin", data_id_admin, " admin : ", type(data_id_admin))
                flash('Editer un admin')
                return redirect(url_for('admin_afficher'))
        except ( Exception,
                 pymysql.err.OperationalError,
                 pymysql.ProgrammingError,
                 pymysql.InternalError,
                 pymysql.IntegrityError,
                 TypeError) as erreur:

            print(erreur.args)
            flash(f"problème admin update {erreur.args[0]}")
            # en cas de problème, mais surtout en cas de non respect
            # des règles REGEX dans le champ "name_edit_intitule_type_html"
            return render_template('admin/admin_edit.html', data=valeur_edit_list)

    return render_template("admin/admin_update.html")
# Select delete
@obj_mon_application.route('/admin_select_delete', methods=['POST','GET'])
def admin_select_delete():
    print("select delete")
    if request.method == 'GET':
        print("if")
        try:
            print("try")
            print("OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.")
            obj_actions_admin = GestionAdmin()
            # OM 2019.04.04 Récupérer la valeur de "idGenreDeleteHTML" du formulaire html "GenresDelete.html"
            id_admin_delete = request.args.get('id_admin_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_admin": id_admin_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_admin = obj_actions_admin.delete_select_admin_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur admin_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur admin_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('admin/admin_delete.html',data=data_id_admin)
# Delete admin
@obj_mon_application.route('/admin_delete', methods=['POST','GET'])
def admin_delete():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_admin = GestionAdmin()
            # OM 2019.04.02 Récupérer la valeur de "id_TypeExercice" du formulaire html "TypeAfficher.html"
            id_admin_delete = request.form['id_admin_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_admin": id_admin_delete}
            print("before delete")
            data_admin = obj_actions_admin.delete_admin_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des people d'admin
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"
            print("after delete")
            # On affiche les people
            return redirect(url_for('admin_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "people" d'admin' qui est associé dans "t_avoirType".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des types !')
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce admin est associé à des types dans la t_avoirType !!! : {erreur}")
                # Afficher la liste des genres des films
                return redirect(url_for('admin_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur type_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur type_delete {erreur.args[0], erreur.args[1]}")
    return render_template('admin/admin_afficher.html', data=data_admin)

