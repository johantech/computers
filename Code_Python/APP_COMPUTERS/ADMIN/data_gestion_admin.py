#data_gestion_admin.py
# JC 2020.04.21 Permet de gérer l'affichage des données de la table t_exercice

from flask import flash

from Code_Python.APP_COMPUTERS.DATABASE import connect_db_context_manager
from Code_Python.APP_COMPUTERS import obj_mon_application
from Code_Python.APP_COMPUTERS.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from Code_Python.APP_COMPUTERS.DATABASE.erreurs import *

class GestionAdmin():
    def __init__(self):
        try:
            print("dams le try de gestion admin")
            # La connexion à la BD est-elle active
            # Rnevoie une erreur si la connexion est perdue
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion admin ...terrible erreur, il faut conencter une base de donnée", "danger")
            # Debug bon marché : Pour afficher un message dans la console
            print(f"Exception grave Classe constructeur GestionAdmin {erreur.args[0]}")
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

        print("Classe constructeur GestionAdmin ")

    def admin_afficher_data(selfs):
        try:
            # selection des champs à afficher
            # permet de lever une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            strsql_admin_afficher = """SELECT id_admin, user, email, password, firstName, lastName, date_creation from admin"""
            print("STRSQL: ", strsql_admin_afficher)
            # Comme on utilise un context manager, on va accéder au cursor avec with
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # on envoie la commande sql
                mc_afficher.execute(strsql_admin_afficher)
                # on récupère les données de la requête
                data_admin = mc_afficher.fetchall()
                # affichage dans la console
                print("data_admin ",data_admin, " Type : ", type(data_admin))
                # retourne les données du "SELECT"
                return data_admin
        except pymysql.Error as erreur:
            print(f"DGF gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(f"DGG fad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGF gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG fad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # raise MaBdErreurDoublon(f"{msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")
            raise MaBdErreurConnexion(f"DGF fad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

# JC 2020.04.30 Ajout des différentes fonctions CUD.

    def add_admin_data(self, valeurs_insertion_dictionnaire):
        try:
            strsql_insert_admin = "INSERT  INTO admin (id_admin,user,email,password,firstName,lastName) VALUES (NULL, %(value_user)s,%(value_email)s,%(value_password)s,%(value_firstName)s,%(value_lastName)s)"
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_admin,valeurs_insertion_dictionnaire)

        except  pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def edit_admin_data(self,valeur_id_dictionnaire):
        try:
            print("BUG",valeur_id_dictionnaire)
            #Commande sql pour afficher le admin sélectionnée dans le tableau dans le formulaire HTML
            str_sql_id_admin = "SELECT id_admin,user,email,password,firstName,lastName FROM admin WHERE id_admin = %(value_id_admin)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_admin, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("str_sql_id_admin", data_one)
                    return data_one
        except Exception as erreur:
            # JC 2020.04.21 MEssage en cas d'échec du bon dérouelement des commandes ci-dessus
            print(f"Problème edit_admin_data Data Gestion Exercice numéro de l'erreur : {erreur}", "danger")
            # On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception...Problème edit_admin_data d'un admin Data Gestion Exercice {erreur}")

    def update_admin_data(self, valeur_update_dictionnaire):
        try:
            print("DEBUG",valeur_update_dictionnaire)
            # JC 2020.04.21 Commande MySql pour la modification de la valeur tapée au clavier dans le champ "nameEditTypeHTML" du form HTML "people_edit.html"
            # le %s permet d'éviter les injection sql simples

            # commande sql pour afficher le people sélectionné dans le tableau dans le formulaire HTML
            str_sql_update_id_admin = "UPDATE admin SET user = %(user)s,"\
                                         "email = %(email)s,"\
                                         "password = %(password)s," \
                                         "firstName = %(firstName)s,"\
                                         "lastName = %(lastName)s"\
                                         " WHERE id_admin = %(id_admin)s"
            print("str_sql_update_id_admin = ",str_sql_update_id_admin )

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_id_admin, valeur_update_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus
            print(f"Problème update_admin_data Data Gestion Exercice numéro de l'erreur : {erreur}")

            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # 2 manière de communiquer une erreur causée par l'insertion d'une valeur à double
                flash("Doublo !! Introduire une valeur différente")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus
                print(f"Problème update_admin_data Data Gestion Admin numéro de l'erreur : {erreur}")

                raise Exception("Raise exception...Problème update_admin_data d'un admin DataGestionAdmin{erreur}")

    def delete_select_admin_data(self, valeur_delete_dictionnaire):
        print("before try")
        try:
            print("valeur dico BUUUUUUUGGGGGGGEEEEEEERRRRR ",valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "ExerciceEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_admin = "SELECT id_admin,user,email,password,firstName,lastName FROM admin WHERE id_admin = %(value_id_admin)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une gméthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_admin, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_exercice_data Gestions Exercice numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_admin_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_admin_data d\'un admin Data Gestions Admin {erreur}")

    def delete_admin_data(self, valeur_delete_dictionnaire):
        try:
            print("valeur dico DEBUUUUGGGGGGGGEEEEEEEEEEEERRRRRRR ", valeur_delete_dictionnaire)
            # JC 2019.04.21 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "admin_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_admin = "DELETE FROM admin WHERE id_admin = %(value_id_admin)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_admin, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_admin_data Data Gestions Type numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Exercice numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un admin qui est associé à un people dans la table intermédiaire "t_avoirtype"
                # il y a une contrainte sur les FK de la table intermédiaire "t_avoirtype"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Cet admin est associé à des types dans la t_avoirtype !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Cet admin est associé à des types dans la t_avoirtype !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
